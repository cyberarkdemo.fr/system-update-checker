import paramiko
from logger import Logger

paramiko.common.logging.basicConfig(level=paramiko.common.ERROR)

class Target:
    def __init__(self, address, username, password):
        self.address = address
        self.username = username
        self.password = password
        self._client = None

    @property
    def client(self):
        if not self._client:

            logger = Logger()
            self._client = paramiko.SSHClient()
            self._client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

            try:
                self._client.connect(hostname=self.address,
                                     username=self.username,
                                     password=self.password)
                logger.info(f'Authentication succeeded for {self.username}@{self.address} ')

            except (paramiko.AuthenticationException,
                    paramiko.ssh_exception.NoValidConnectionsError) as e:
                logger.error(f'{self.username}@{self.address}: {e} ')

        return self._client

    def run(self, cmd):
        stdin, stdout, stderr = self.client.exec_command(cmd)
        out = stdout.read().decode().strip()
        error = stderr.read().decode().strip()

        if error:
            raise Exception(f'[ERROR] There was an error pulling the runtime: {error}')

        return out
