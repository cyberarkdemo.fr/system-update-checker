#!/usr/bin/env python3

import csv

from logger import Logger
from target import Target
from cp import CP
import argparse


def get_options():
    description = 'cyberarkdemo.fr - System Update Checker'
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument('--mode',
                        choices=['csv', 'cyberark'],
                        help='Load configuration from csv or cyberark',
                        default='csv',
                        type=str)
    parser.add_argument('--csv',
                        default='targets.csv',
                        type=str)

    parser.add_argument('--appid',
                        type=str)

    parser.add_argument('-d', '--debug', action='store_true')
    parser.add_argument('-e', '--error', action='store_true')
    parser.add_argument('-t', '--trace', action='store_true')

    return parser.parse_args()


def main():
    logger = Logger()

    #######################################
    #      Create the list of targets     #
    #######################################
    targets = []
    if options.mode == 'csv':
        with open(options.csv, newline='') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=',', quotechar='|')
            for row in reader:
                logger.debug(f'Register {row["username"]}@{row["address"]}')

                targets.append(
                    Target(row["address"], row["username"], row["password"])
                )
    elif options.mode == 'cyberark':
        with open(options.csv, newline='') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=',', quotechar='|')
            cp = CP(options.appid)
            for row in reader:
                logger.debug(f'Fetching {row["name"]} in {row["safe"]}')
                account = cp.get(row["safe"], row['name'])

                logger.debug(f'Register {account.username}@{account.address} with password {account.password}')
                targets.append(
                    Target(account.address, account.username, account.password)
                )

    #######################################
    #        Connect to each target       #
    #######################################
    for target in targets:
        out = target.run('echo "Hello world"')
        logger.info(f'{target.username}@{target.address}: {out}')


if __name__ == '__main__':

    options = get_options()

    log_level = 'info'
    if options.debug:
        log_level = 'debug'
    if options.error:
        log_level = 'error'
    if options.trace:
        log_level = 'trace'

    logger = Logger(log_level)
    logger.info("==========================================")
    logger.info("========  System Update Checker  =========")
    logger.info("==========  By cyberarkdemo.fr  ==========")
    logger.info("==========================================\n")

    main()
