# System Update Checker

System Update Checker is a demo application meant to illustrate a Cyberark CP integration.
DO NOT USE THIS APPLICATION TO VERIFY UPDATES ON A SERVER.

## Prerequisites

Install project dependencies:
```bash
$ pip install -r requirements.txt 
```

## Usage

System Update Checker can be used in two modes:
- `csv` (default) - target list is loaded from a csv file
- `cyberark` - target list is loaded from cyberark cp

You must specify the mode you want to use with the option `--mode <mode>`.
You must specify the file to load with the option `--csv <filename>`.

Examples:
```bash
# Run in csv mode using targets.csv
$ ./main.py --mode csv --csv targets.csv

# Run in cyberark mode (CP) using targets-cyberark.csv
$ ./main.py --appid <CYBERARK APPID> --mode cyberark --csv targets-cyberark.csv 
```

## Licensing

Copyright © 2021 CyberArk Software Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
