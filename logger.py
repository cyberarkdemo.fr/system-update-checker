from singleton import Singleton

DEBUG_LEVEL_INFO = 'info'    # Outputs errors and info.
DEBUG_LEVEL_DEBUG = 'debug'  # Outputs debug information
DEBUG_LEVEL_TRACE = 'trace'  # Outputs all information


class Logger(metaclass=Singleton):
    def __init__(self, debug_level=DEBUG_LEVEL_INFO):
        self.debug_level = debug_level.lower()

    def info(self, message):
        message = str(message)
        if self.debug_level in [DEBUG_LEVEL_INFO, DEBUG_LEVEL_DEBUG, DEBUG_LEVEL_TRACE]:
            print(f'[INFO] {message}')

    def error(self, message):
        message = str(message)
        if self.debug_level in [DEBUG_LEVEL_INFO, DEBUG_LEVEL_DEBUG, DEBUG_LEVEL_TRACE]:
            print(f'[ERROR] {message}')

    def debug(self, message):
        message = str(message)
        if self.debug_level in [DEBUG_LEVEL_DEBUG, DEBUG_LEVEL_TRACE]:
            print(f'[DEBUG] {message}')

    def trace(self, *args, caller_name):
        args = [str(arg) for arg in args]
        if self.debug_level.lower() == DEBUG_LEVEL_TRACE:
            print(f'[TRACE] {caller_name}: ', args, sep=' | ')
