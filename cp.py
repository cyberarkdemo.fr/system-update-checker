from subprocess import PIPE, run
from logger import Logger
import os

class CPAccount:
    def __init__(self, username, address, password):
        self.username = username
        self.address = address
        self.password = password


class CP:
    def __init__(self, app_id):
        self.app_id = app_id
        self.logger = Logger()
        pass

    def out(self, command, workdir=None):
        result = run(command,
                     stdout=PIPE,
                     stderr=PIPE,
                     universal_newlines=True,
                     shell=True,
                     cwd=workdir
                     )

        if result.returncode != 0:
            self.logger.error(f"RETURN CODE: {result.returncode}")
            self.logger.error(f"STDOUT: {result.stdout}")
            self.logger.error(f"STDERR: {result.stderr}")
            exit('Fatal error: Failed to fetch password from CP')

        return [result.stdout, result.stderr]

    def get(self, safe, account_name):
        stdout, _ = self.out(f'bash $PWD/cp.sh "{self.app_id}" "{safe}" "{account_name}"', os.getcwd())

        output = stdout.replace('\n','').split(',')
        return CPAccount(output[0], output[1], output[2])
